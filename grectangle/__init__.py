from kivy.lang import Builder
from kivy import properties as kp

from base import Component, Container


Builder.load_file('grectangle/grectangle.kv')


gratio = 1.61803398875


class GRectangle(Component):
    pass


class GoldenRectangle(GRectangle, Container):
    ps0 = {'x': 1 / gratio, 'y': 0}
    ps1 = {'x': 0, 'y': 0}
    ps2 = {'x': 0, 'y': 1 / gratio}

    sh0 = (1 / gratio, 1)
    sh1 = (1, 1 / gratio)

    def _fill_up(self):
        pos = 'ps{}'.format(self.level % 3)
        self.add_new_component(pos)

    def default_kwargs(self, psh):
        index = int(psh[-1]) % 2
        return {'size_hint': getattr(self, 'sh{}'.format(index)),
                'pos_hint': getattr(self, psh),
                'level': self.level + 1}

