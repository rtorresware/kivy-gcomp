from kivy import properties as kp
from kivy.lang import Builder

from base import Component, Container


Builder.load_file('flower/flower.kv')


class FlowerBase(Component):
    color1 = kp.ListProperty([0, 0, 0, 0])
    color2 = kp.ListProperty([0, 0, 0, 0])


class Flower(Container, FlowerBase):
    child_size_hint = (.58, .58)
    ps1 = {'center_y': .5, 'center_x': .5}

    def _fill_up(self):
        self.add_new_component('ps1')


