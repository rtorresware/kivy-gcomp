from kivy import properties as kp
from kivy.lang import Builder

from base import Component, Container


Builder.load_file('cubes/cubes.kv')


class Cube(Component):
    pass


class FinalCube(Cube):
    pass


class Cubes(Container, Cube):
    ps0 = {'x': 0, 'y': .375}
    ps1 = {'x': .5, 'y': .375}
    ps2 = {'x': .25, 'y': 0}

    child_size_hint = (.5, .5)

    def _fill_up(self):
        for index, pos in ((index, 'ps{}'.format(index))
                           for index in range(3)):
            cube_cls = FinalCube
            if self.level != self.levels and not (index + self.level) % 3:
                cube_cls = Cubes
            self.add_new_component(pos, cube_cls)


class Cubes2(Cubes):
    def _fill_up(self):
        for index, pos in ((index, 'ps{}'.format(index))
                           for index in range(3)):
            cube_cls = FinalCube
            if self.level != self.levels:
                cube_cls = Cubes2
            self.add_new_component(pos, cube_cls)

