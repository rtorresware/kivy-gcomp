from kivy.app import App
from kivy import properties as kp

from cubes import Cubes, Cubes2
from sierpinski import SierpinskiTriangle
from grectangle import GoldenRectangle
from flower import Flower


LEVELS = 0
CONTAINERS = (Flower, Cubes, Cubes2, SierpinskiTriangle)


class Drawer(App):
    levels = kp.NumericProperty(LEVELS)

    containers = kp.ListProperty(CONTAINERS)
    container_cls = kp.ObjectProperty(CONTAINERS[0])

    def on_levels(self, *args):
        self.fill()

    def on_container_cls(self, *args):
        self.fill()

    def fill(self):
        self.container_cls.levels = self.levels
        container = self.container_cls()
        self.root.ids.box.clear_widgets()
        self.root.ids.box.add_widget(container)

    def on_start(self):
        self.fill()


Drawer().run()

