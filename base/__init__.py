from kivy.uix.floatlayout import FloatLayout
from kivy import properties as kp


class Component(FloatLayout):
    level = kp.NumericProperty(0)
    line_width = kp.NumericProperty(.5)


class Container(Component):
    levels = 0
    child_size_hint = (1, 1)

    def default_kwargs(self, psh):
        return {'size_hint': self.child_size_hint,
                'pos_hint': getattr(self, psh),
                'level': self.level + 1}

    def add_new_component(self, psh, cls=None, **kwargs):
        cls = cls or self.__class__
        new_kwargs = self.default_kwargs(psh)
        new_kwargs.update(kwargs)
        self.add_widget(cls(**new_kwargs))

    def __init__(self, **kwargs):
        super(Container, self).__init__(**kwargs)
        self.fill_up()

    def fill_up(self):
        self.clear_widgets()
        if self.level < self.levels:
            self._fill_up()

    def _fill_up(self):
        pass

