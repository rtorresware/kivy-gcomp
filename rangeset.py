class Range:
	def __init__(self, start, end):
		self.start = start
		self.end = end

	def __in__(self, member):
		return self.start <= member <= self.end
	

class RangeSet:

	def __init__(self):
		# collection of lists like so [11, 12]
self._ranges = []		

	def add(self, start=None, end=None, range=None):
		if not range:
			range = Range(start, end)
		rwce = self._get_range_which_contains(end)
		rwcs = self._get_range_which_contains(start)
		if rwce and rwcs:
			return 
		if rwcs and not rwce:
			rwcs.end = end
			return
		if not rwcs and rwce:
			rwce.start = start
			return
		self._ranges.append(range)
		
	def __in__(self, member):
		return bool(self._get_range_which_contains(member))

	def _get_range_which_contains(self, member):
		for test_range in self._ranges:
			if member in test_range:
				return test_range
		return None

