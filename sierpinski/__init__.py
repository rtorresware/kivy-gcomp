from kivy import properties as kp
from kivy.lang import Builder

from base import Component, Container


Builder.load_file('sierpinski/sierpinski.kv')


class SierTriangle(Component):
    pass


class SierpinskiTriangle(Container, SierTriangle):
    ps0 = {'x': 0, 'y': 0}
    ps1 = {'x': .5, 'y': 0}
    ps2 = {'x': .25, 'y': .5}

    child_size_hint = (.5, .5)

    def _fill_up(self):
        for pos in ('ps{}'.format(x) for x in range(3)):
            self.add_new_component(pos)

